import * as React from 'react';
import {CoveoReactResultTemplate} from "coveoforsitecore-jss";

const templateNameField = `z95xtemplatename`

const rawTemplate = `<div>
    <a class='CoveoResultLink'><%= title ? highlight(title, titleHighlights) : clickUri %></a>
    <div class="CoveoFieldValue" data-field="{{= coveoFieldName("@${templateNameField}") }}"></div>
</div>`;

// Example filter
const filters = {};
filters[templateNameField] = "Sample Item";

const CustomTemplate = () => (
    <CoveoReactResultTemplate filters={filters} rawTemplate={rawTemplate} />
)

export default CustomTemplate;
